import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { SupermercadoServiceProvider } from '../../providers/supermercado-service/supermercado-service';

@Component({
  selector: 'page-lista-de-supermercados',
  templateUrl: 'lista-de-supermercados.html',
  providers: [SupermercadoServiceProvider]
})

export class ListaDeSupermercadosPage {
  public Supermercados: any;

  constructor(public navCtrl: NavController, private SupermercadosService: SupermercadoServiceProvider) {
    this.loadSupermercado(); 
  }

  loadSupermercado(){
        this.SupermercadosService.load()
          .then(data => {
            this.Supermercados = data;
          })
    }
}
