import { ProdutoServiceProvider } from './../../providers/produto-service/produto-service';
import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { LocalizaOPage } from '../localiza-o/localiza-o';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
  providers: [ProdutoServiceProvider]
  
})
export class HomePage {

  public Produtos: any;

  //items;

  constructor(public navCtrl: NavController, private ProdutoService: ProdutoServiceProvider) {
    //this.initializeItems();
    this.loadProduto();
  }

  loadProduto(){
    this.ProdutoService.load()
      .then(data =>{
        this.Produtos = data;
      })
  }

  /*initializeItems() {
    this.items = [];
  }

  getItems(ev) {
    // Reset items back to all of the items
    this.initializeItems();

    // set val to the value of the ev target
    var val = ev.target.value;

    // if the value is an empty string don't filter the items
    if (val && val.trim() != '') {
      this.items = this.items.filter((item) => {
        return (item.toLowerCase().indexOf(val.toLowerCase()) > -1);
      })
    }
  }*/

  goToLocalizaO(params){
    if (!params) params = {};
    this.navCtrl.push(LocalizaOPage);
  }
}
