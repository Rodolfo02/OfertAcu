import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { ListaDeSupermercadosPage } from '../pages/lista-de-supermercados/lista-de-supermercados';
import { SobrePage } from '../pages/sobre/sobre';
import { LocalizaOPage } from '../pages/localiza-o/localiza-o';
import { HttpModule } from '@angular/http';


import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { SupermercadoServiceProvider } from '../providers/supermercado-service/supermercado-service';
import { ProdutoServiceProvider } from '../providers/produto-service/produto-service';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    ListaDeSupermercadosPage,
    SobrePage,
    LocalizaOPage,
  ],
  imports: [
    BrowserModule,
    HttpModule,
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    ListaDeSupermercadosPage,
    SobrePage,
    LocalizaOPage,
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    SupermercadoServiceProvider,
    ProdutoServiceProvider
  ]
})
export class AppModule {}